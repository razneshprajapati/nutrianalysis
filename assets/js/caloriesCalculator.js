function calculateData() {

    let age = parseFloat(document.querySelector("input.age").value);
    //let unit = form.distance_unit.value;
    if (isNaN(age) || age < 0) {
        return alert('Please enter a valid age');
    }

    // Height


    let heightFeet = parseFloat(document.querySelector("input.height_ft").value);
    if (isNaN(heightFeet) || heightFeet < 0) {
        return alert('Please enter a valid Height in feet');
    }
    let heightInches = parseFloat(document.querySelector("input.height_in").value);
    if (isNaN(heightInches) || heightInches < 0) {
        heightInches = 0;
    }
    heightCM = (2.54 * heightInches) + (30.4 * heightFeet)



    let weight = parseFloat(document.querySelector("input.weight").value);
    if (isNaN(weight) || weight < 0) {
        return alert('Please enter a valid weight');
    }



    let calories = 0;
    if (document.querySelector("select.gender").value == 'Female') {
        //females =  655.09 + 9.56 x (Weight in kg) + 1.84 x (Height in cm) - 4.67 x age   
        calories = 655.09 + (9.56 * weight) + (1.84 * heightCM) - (4.67 * age);
    } else {
        calories = 66.47 + (13.75 * weight) + (5 * heightCM) - (6.75 * age);
    }

    document.querySelector("div.caloriesDisplay").innerHTML = "Calories : " + parseFloat(calories).toFixed(2);
}