
function addFood() {
    var food_name = document.querySelector("input.foodname").value;
    var nutri_data_send = [];
    fetch("https://api.edamam.com/api/food-database/parser?app_id=ca747d07&app_key=722fabaee32b8118f7b1cb2e32b137cf&ingr=" + food_name)
        .then(resp => resp.json())
        .then(resp => {
            nutri_data_send[0] = resp.hints[0].food.nutrients["CHOCDF"] >= 0 ? resp.hints[0].food.nutrients["CHOCDF"] : 0;
            nutri_data_send[1] = resp.hints[0].food.nutrients["ENERC_KCAL"] >= 0 ? resp.hints[0].food.nutrients["ENERC_KCAL"] : 0;
            nutri_data_send[2] = resp.hints[0].food.nutrients["FAT"] >= 0 ? resp.hints[0].food.nutrients["FAT"] : 0;
            nutri_data_send[3] = resp.hints[0].food.nutrients["PROCNT"] >= 0 ? resp.hints[0].food.nutrients["PROCNT"] : 0;
            nutri_data_send[4] = food_name;
            // console.log(nutri_data);

            //data should be fetch first to send it in url
            xmlhttp_tabledata.open("GET", "getdata.php?q=" + JSON.stringify(nutri_data_send) + "&u_id="+sessionStorage.getItem("u_id")+"&caller=setData", true); //JSON.stringify() is use to send array
            xmlhttp_tabledata.send();
            

        });
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp_tabledata = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp_tabledata = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp_tabledata.onreadystatechange = function () {
        if (xmlhttp_tabledata.readyState == 4 && xmlhttp_tabledata.status == 200) {
            var e = new Date;
            var t = e.getFullYear();
            var n = e.getMonth() + 1;
            var n1 = n <= 9 ? "0" + n : n;
            var dd = e.getDate();
            var d1 = dd <= 9 ? "0" + dd : dd;
            var fulldate = t + "-" + n1 + "-" + d1;

            showChart(t, n, dd);
            document.querySelector("div.table_display").innerHTML = xmlhttp_tabledata.responseText;
            console.log(document.querySelector("div.table_display"));
        }
    }

    var e = new Date;
    var t = e.getFullYear();
    var n = e.getMonth() + 1;
    var n1 = n <= 9 ? "0" + n : n;
    var dd = e.getDate();
    var d1 = dd <= 9 ? "0" + dd : dd;
    var fulldate = t + "-" + n1 + "-" + d1;


    if (window.XMLHttpRequest) {
        xmlhttp_dailydata = new XMLHttpRequest();
    } else {
        xmlhttp_dailydata = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp_dailydata.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var nutri_data = eval(xmlhttp_dailydata.responseText);
            //console.log("values:::::"+nutri_data);
            var ctxD = null;

            //removing hover effect
            document.querySelector("canvas.doughnutChart").remove();
            document.querySelector("div.doughnautDisplay").innerHTML = "<canvas class='doughnutChart'></canvas>";

            //reference of canvas and context
            var canvas = document.querySelector("canvas.doughnutChart");
            ctxD = canvas.getContext('2d');
            // canvas.width = 280;
            // canvas.height = 155;

            if (nutri_data[0] == "0", nutri_data[1] == "0", nutri_data[2] == "0", nutri_data[3] == "0") {
                ctxD.font = "30px Comic Sans MS";
                ctxD.fillStyle = "black";
                ctxD.textAlign = "center";
                ctxD.fillText("Data not found!!!", canvas.width / 2, canvas.height / 2);
            }
            else {

                var myLineChart = new Chart(ctxD, {
                    type: 'bar',
                    data: {
                        labels: ["Carbohydrate", "Energy", "Fat", "Protine"],
                        datasets: [{
                            barPercentage: 0.3,
                            barThickness: 40,
                            maxBarThickness: 50,
                            minBarLength: 2,
                            data: nutri_data,
                            backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1"],
                            hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5"],

                        }]
                    },
                    options: {
                        legend: {
                            display: false,
                        },
                        title: {
                            display: true,
                            text: 'Daily Nutrition Analysis'
                        }
                    }
                });
                ctxD.clearRect(0, 0, canvas.width, canvas.height);
            }
            nutri_data = [];


        }
    }

    // xmlhttp_dailydata.open("GET", "getdata.php?fulldate=" + fulldate + "&u_id=1&caller=getDailyNutri", true);
    // xmlhttp_dailydata.send();



}
