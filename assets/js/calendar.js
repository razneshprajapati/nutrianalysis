$(function () {
    function c() {
        p();
        var e = h();
        var r = 0;
        var u = false;
        l.empty();
        while (!u) {
            if (s[r] == e[0].weekday) {
                u = true
            }
            else {
                l.append('<div class="blank"></div>'); r++
            }
        }
        for (var c = 0; c < 42 - r; c++) {
            if (c >= e.length) {
                l.append('<div class="blank"></div>')
            }
            else {
                var v = e[c].day;
                var m = g(new Date(t, n - 1, v)) ? '<div onclick="showChart(' + t + ',' + n + ',' + v + ')" id="' + t + '-' + n + '-' + v + '" class="today">' :
                    "<div onclick='showChart(" + t + "," + n + "," + v + ")' id='" + t + "-" + n + "-" + v + "'>";
                l.append(m + "" + v + "</div>")
            }
        }
        var y = o[n - 1];
        a.css("background-color", y).find("h1").text(i[n - 1] + " " + t);
        f.find("div").css("color", y);
        l.find(".today").css("background-color", y);
        d()
    }
    function h() {
        var e = [];
        for (var r = 1; r < v(t, n) + 1; r++) {
            e.push({ day: r, weekday: s[m(t, n, r)] })
        }
        return e
    }

    function p() {
        f.empty();
        for (var e = 0; e < 7; e++) {
            f.append("<div>" + s[e].substring(0, 3) + "</div>")
        }
    }

    function d() {
        var t;
        var e = 267;
        var mp = 1.23;
        var n = $("#calendar").css("width", e * mp + "px");
        n.find(t = "#calendar_weekdays, #calendar_content")
            .css("width", e * mp + "px")
            .find("div")
            .css({ width: e * mp / 7 + "px", height: e / 7 + "px", "line-height": e / 7 + "px" });
        n.find("#calendar_header")
            .css({ height: e * (1 / 10) + "px" })
            .find('i[class^="icon-chevron"]')
            .css("line-height", e * (1 / 10) + "px")
    }

    function v(e, t) {
        return (new Date(e, t, 0)).getDate()
    }

    function m(e, t, n) {
        return (new Date(e, t - 1, n)).getDay()
    }

    function g(e) {
        return y(new Date) == y(e)
    }

    function y(e) {
        return e.getFullYear() + "/" + (e.getMonth() + 1) + "/" + e.getDate()
    }

    function b() {
        var e = new Date;
        t = e.getFullYear();
        n = e.getMonth() + 1
    }

    var e = 480;
    var t = 2013;
    var n = 9;
    var r = [];
    var i = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"];
    var s = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var o = ["#16a085", "#1abc9c", "#c0392b", "#27ae60", "#FF6860", "#f39c12", "#f1c40f", "#e67e22", "#2ecc71", "#e74c3c", "#d35400", "#2c3e50"];
    var u = $("#calendar");
    var a = u.find("#calendar_header");
    var f = u.find("#calendar_weekdays");
    var l = u.find("#calendar_content");
    b();
    c();
    a.find('i[class^="icon-chevron"]').on("click", function () {
        var e = $(this);
        var r = function (e) {
            n = e == "next" ? n + 1 : n - 1;
            if (n < 1) {
                n = 12;
                t--
            } else if (n > 12) {
                n = 1;
                t++
            }
            c()
        };
        if (e.attr("class").indexOf("left") != -1) {
            r("previous")
        } else {
            r("next")
        }
    })

    var todayChart = document.querySelector("div.today").id.split('-');
    showChart(todayChart[0], todayChart[1], todayChart[2]);
})




let fulldateid2 = []; //for storing previous date value
function showChart(year, month, day) {
    var year = year;
    var m = month;
    var d = day;
    var month = month <= 9 ? "0" + month : month;
    var day = day <= 9 ? "0" + day : day;
    var fulldate = year + "-" + month + "-" + day;
    var fulldateid = year + "-" + m + "-" + d;

    var e = new Date;
    var t = e.getFullYear();
    var n = e.getMonth() + 1;
    var n1 = n <= 9 ? "0" + n : n;
    var dd = e.getDate();
    var d1 = dd <= 9 ? "0" + dd : dd;
    var today = t + "-" + n1 + "-" + d1;
    var today2 = t + "-" + n + "-" + dd;

    if (fulldateid2.length != 0) {
        if (fulldateid2[0].localeCompare(today2) != 0) {
            console.log("prv date: " + fulldateid2[0]);
            if (document.getElementById(fulldateid2[0]) != null)
                document.getElementById(fulldateid2[0]).style.background = "white";
            document.getElementById(fulldateid2[0]).style.color = "grey";
            //backgoround of previous selected date
        }
        fulldateid2.pop();
    }


    if (fulldate.localeCompare(today) != 0) {
        document.getElementById(fulldateid).style.background = "grey"; //background of current selected date
        document.getElementById(fulldateid).style.color = "ghostwhite"; //background of current selected date
        //console.log("selected date: "+document.getElementById(fulldateid).id);
    }


    //chart display using calendar

    if (window.XMLHttpRequest) {
        xmlhttp_calendar = new XMLHttpRequest();
    } else {
        xmlhttp_calendar = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp_calendar.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var nutri_data = [];
            nutri_data = eval(xmlhttp_calendar.responseText);
            //console.log("values:::::"+nutri_data);
            var ctxD = null;

            //removing hover effect
            document.querySelector("canvas.doughnutChart").remove();
            document.querySelector("div.doughnautDisplay").innerHTML = "<canvas class='doughnutChart'></canvas>";

            //reference of canvas and context
            var canvas = document.querySelector("canvas.doughnutChart");
            ctxD = canvas.getContext('2d');
            // canvas.width = 280;
            // canvas.height = 155;

            if (nutri_data[0] == "0", nutri_data[1] == "0", nutri_data[2] == "0", nutri_data[3] == "0") {
                ctxD.font = "30px Comic Sans MS";
                ctxD.fillStyle = "black";
                ctxD.textAlign = "center";
                ctxD.fillText("Data not found!!!", canvas.width / 2, canvas.height / 2);
            }
            else {

                var myLineChart = new Chart(ctxD, {
                    type: 'bar',
                    data: {
                        labels: ["Carbohydrate", "Energy", "Fat", "Protine"],
                        datasets: [{
                            barPercentage: 0.3,
                            barThickness: 40,
                            maxBarThickness: 50,
                            minBarLength: 2,
                            data: nutri_data,
                            backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1"],
                            hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5"],

                        }]
                    },
                    options: {
                        legend: {
                            display: false,
                        },
                        title: {
                            display: true,
                            text: 'Daily Nutrition Analysis'
                        }
                    }
                });
                ctxD.clearRect(0, 0, canvas.width, canvas.height);
            }
            nutri_data = [];
            //console.log(fulldateid2);
            fulldateid2.push(fulldateid);
        }
    }
    console.log(fulldate);
    var fulldatex = fulldate;
    xmlhttp_calendar.open("GET", "getdata.php?fulldate=" + fulldate + "&u_id=" + sessionStorage.getItem("u_id") + "&caller=getDailyNutri", true); //JSON.stringify() is use to send array
    xmlhttp_calendar.send();



    //getting data for the table

    if (window.XMLHttpRequest) {
        xmlhttp_getTable = new XMLHttpRequest();
    } else {
        xmlhttp_getTable = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp_getTable.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.querySelector("div.table_display").innerHTML = this.responseText;
        }
    }

    xmlhttp_getTable.open("GET", "getdata.php?fulldate=" + fulldate + "&u_id=" + sessionStorage.getItem("u_id") + "&caller=getTable", true);
    xmlhttp_getTable.send();




    var e = new Date;
    var t = e.getFullYear();
    var n = e.getMonth() + 1;
    var n1 = n <= 9 ? "0" + n : n;
    var dd = e.getDate();
    var d1 = dd <= 9 ? "0" + dd : dd;
    var fulldate = t + "-" + n1 + "-" + d1;



    if (window.XMLHttpRequest) {
        xmlhttp_weekgraph = new XMLHttpRequest();
    } else {
        xmlhttp_weekgraph = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp_weekgraph.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var all_data = null;

            var nutri_data = [];
            nutri_data["crab"] = [];
            nutri_data["ene"] = [];
            nutri_data["fat"] = [];
            nutri_data["pro"] = [];



            var ctxD = null;
            var labels = [];

            //removing hover effect
            document.querySelector("canvas.weeklyChart").remove();
            document.querySelector("div.weeklyDisplay").innerHTML = "<canvas class='weeklyChart'></canvas>";

            //reference of canvas and context
            var canvas = document.querySelector("canvas.weeklyChart");
            ctxD = canvas.getContext('2d');


            all_data = eval(xmlhttp_weekgraph.responseText);
            console.log(all_data);
            for (var i = 0; i <= 6; i++) {
                for (var label in all_data[i]) {
                    labels.push(label);
                    for (var x in all_data[i][label]) {
                        nutri_data[x].push(all_data[i][label][x]);
                    }
                }
            }

            var myLineChart = new Chart(ctxD, {
                type: 'bar',

                data: {
                    labels: labels,
                    datasets: [{
                        label: "Carbohydrate",
                        barPercentage: 0.3,
                        barThickness: 40,
                        maxBarThickness: 50,
                        minBarLength: 2,
                        data: nutri_data["crab"],
                        backgroundColor: "#F7464A",
                        hoverBackgroundColor: "#FF5A5E",
                    },
                    {
                        label: "Energy",
                        barPercentage: 0.3,
                        barThickness: 40,
                        maxBarThickness: 50,
                        minBarLength: 2,
                        data: nutri_data["ene"],
                        backgroundColor: "#46BFBD",
                        hoverBackgroundColor: "#5AD3D1",
                    },
                    {
                        label: "Fat",
                        barPercentage: 0.3,
                        barThickness: 40,
                        maxBarThickness: 50,
                        minBarLength: 2,
                        data: nutri_data["fat"],
                        backgroundColor: "#FDB45C",
                        hoverBackgroundColor: "#FFC870",
                    },
                    {
                        label: "Protine",
                        barPercentage: 0.3,
                        barThickness: 40,
                        maxBarThickness: 50,
                        minBarLength: 2,
                        data: nutri_data["pro"],
                        backgroundColor: "#949FB1",
                        hoverBackgroundColor: "#A8B3C5",
                    },

                    ]
                },
                options: {
                    legend: {
                        display: false,
                    },
                    title: {
                        display: true,
                        text: 'Weekly Nutrition Analysis'
                    },
                    scales: {
                        xAxes: [{
                            stacked: true
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    },
                }
            });
            nutri_data = [];
            ctxD.clearRect(0, 0, canvas.width, canvas.height);

        }
    }



    xmlhttp_weekgraph.open("GET", "getdata.php?fulldate=" + fulldate + "&u_id=" + sessionStorage.getItem("u_id") + "&caller=getWeeklyData", true);
    xmlhttp_weekgraph.send();

}
