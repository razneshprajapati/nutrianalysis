var nutri_data = [];

function show() {
    document.querySelector("div.reverse-spinner").style = "visibility:visible";
    var food_name = document.querySelector('textarea.food-name').value;

    var ctxD = null;

    //removing hover effect
    document.querySelector("canvas.doughnutChart").remove();
    document.querySelector("div.doughnautDisplay").innerHTML = "<canvas class='doughnutChart'></canvas>";

    //reference of canvas and context
    var canvas = document.querySelector("canvas.doughnutChart");
    ctxD = canvas.getContext('2d');

    fetch("https://api.edamam.com/api/food-database/parser?app_id=ca747d07&app_key=722fabaee32b8118f7b1cb2e32b137cf&ingr=" +
        food_name)
        .then(resp => resp.json())
        .then(resp => {
            console.log(resp.hints[0]);
            if (resp) {
                document.querySelector("div.reverse-spinner").style =
                    "visibility:hidden;margin:0px;height:0;width:0";
            }
            var multiplier = food_name[0];
            console.log(multiplier+" food name "+food_name);
            nutri_data[0] = multiplier * resp.hints[0].food.nutrients["CHOCDF"];
            nutri_data[1] = multiplier * resp.hints[0].food.nutrients["ENERC_KCAL"];
            nutri_data[2] = multiplier * resp.hints[0].food.nutrients["FAT"];
            nutri_data[3] = multiplier * resp.hints[0].food.nutrients["PROCNT"];


            var myLineChart = new Chart(ctxD, {
                type: 'doughnut',
                data: {
                    labels: ["Carbohydrate", "Energy", "Fat", "Protine"],
                    datasets: [{
                        data: nutri_data,
                        backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1"],
                        hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5"]
                    }]
                },
                options: {
                    responsive: true
                }
            });
            nutri_data = [];
            ctxD.clearRect(0, 0, canvas.width, canvas.height);
            console.log(ctxD);
            console.log(canvas.width);

            console.log(nutri_data);
        })
        .catch(err => {
            console.log(err);
        });

}