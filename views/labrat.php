<html>

<head>
	<title></title>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
	<style type="text/css">
		body {
			margin: 0px;
		}

		.chart_container {
			margin: 0px;
			display: flex;
			flex-direction: column;
			align-items: center;

			width: 100%;
			height: 100vh;
		}

		.nutrition_head {
			margin: auto;
		}

		.food_container {
			display: flex;
			flex-direction: column;
			justify-content: center;
			align-items: center;
			margin-bottom: 8vh;
		}

		.doughnautDisplay {
			display: flex;
			height: 50%;
			width: 50%;
		}

		button {
			height: 30px;
			width: auto;
			margin-top: 4vh;
		}

		.food-name {
			width: 200px;
			height: 30px;
			border-radius: 7px;
		}

		.loader {
			border: 16px solid #f3f3f3;
			border-radius: 50%;
			border-top: 16px solid #3498db;
			width: 115px;
			height: 150px;
			animation: spin 2s linear infinite;
			margin-top: 8vh;
		}

		@keyframes spin {
			0% {
				transform: rotate(0deg);
			}

			100% {
				transform: rotate(360deg);
			}
		}







		ul {
			list-style-type: none;
		}

		input[type=radio] {
			position: absolute;
			left: -9999px;
			top: -9999px;
		}

		.tabs {
			position: relative;
			z-index: 999;
			height: 42px;
			white-space: nowrap;
			font-size: 0;
		}

		.tabs label {
			display: inline-block;
			vertical-align: bottom;
			margin-right: 4px;
			margin-bottom: -1px;
			margin-left: -1px;
			background: #EEE;
			padding: 12px 24px;
			border: solid 1px #DDD;
			border-radius: 5px 5px 0 0;
			cursor: pointer;
			background-color: #EEE;
			text-transform: uppercase;
			font-size: 11px;
			letter-spacing: 1px;
			box-shadow: 0 -1px 6px rgba(0, 0, 0, 0.1) inset;
			color: #666;
		}

		.tabs label:first-child {
			margin-left: 0;
		}

		.tabs label:hover {
			background-color: #DDD;
		}

		input:nth-child(1):checked~.tabs label:nth-child(1),
		input:nth-child(2):checked~.tabs label:nth-child(2),
		input:nth-child(3):checked~.tabs label:nth-child(3),
		input:nth-child(4):checked~.tabs label:nth-child(4) {
			padding: 14px 24px;
			border-bottom-color: #FFF;
			background: #FFF;
			box-shadow: none;
			color: #000;
		}

		.sections {}

		.sections li {
			display: none;
			height: 180px;
			padding: 1em;
			border: solid 1px #DDD;
			border-radius: 0 5px 5px 5px;
			background-color: #FFF;
			box-shadow: 1px 1px 20px rgba(0, 0, 0, 0.4);
		}

		input:nth-child(1):checked~.sections li:nth-child(1),
		input:nth-child(2):checked~.sections li:nth-child(2),
		input:nth-child(3):checked~.sections li:nth-child(3),
		input:nth-child(4):checked~.sections li:nth-child(4) {
			display: block;
		}
	</style>
	<script type="text/javascript">
		var chocdf = 0;
		var enerc_kcal = 0;
		var fat = 0;
		var procnt = 0;
		var nutri_data = [];


		function show() {
			//document.querySelector("div.loader").style="visibility:hidden";
			//var food_name=document.querySelector('input.food-name').value;

			var ctxD = null;
			var labels = [];

			//removing hover effect
			document.querySelector("canvas.doughnutChart").remove();
			document.querySelector("div.doughnautDisplay").innerHTML = "<canvas class='doughnutChart'></canvas>";

			//reference of canvas and context
			var canvas = document.querySelector("canvas.doughnutChart");
			ctxD = canvas.getContext('2d');

			var nutri_data = [];
			nutri_data["crab"] = [];
			nutri_data["ene"] = [];
			nutri_data["fat"] = [];
			nutri_data["pro"] = [];

			console.log(nutri_data);


			/*fetch("https://api.edamam.com/api/food-database/parser?app_id=ca747d07&app_key=722fabaee32b8118f7b1cb2e32b137cf&ingr="+food_name)
				.then(resp => resp.json())
				.then(resp => {
					console.log(resp.hints[0]);
				if(!resp){//if(resp){
					document.querySelector("div.loader").style="visibility:hidden;margin:0px;height:0px;width:0px";
				}
				nutri_data[0]=resp.hints[0].food.nutrients["CHOCDF"];
				nutri_data[1]=resp.hints[0].food.nutrients["ENERC_KCAL"];
				nutri_data[2]=resp.hints[0].food.nutrients["FAT"];
				nutri_data[3]=resp.hints[0].food.nutrients["PROCNT"];

				var nutri_data=[130,400,240,300];
				var myLineChart = new Chart(ctxD, {
					type: 'doughnut',
					data: {
						labels: ["Carbohydrate", "Energy", "Fat", "Protine"],
						datasets: [{
							data: nutri_data,
							backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1"],
							hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5"]
						}]
					},
					options: {
						responsive: true
					}
				});
				nutri_data=[];
				ctxD.clearRect(0, 0, canvas.width, canvas.height);
				console.log(ctxD);
				console.log(canvas.width);
				
				console.log(nutri_data);
			})
			.catch(err => {
				console.log(err);
			});*/


			//week days

			var e = new Date;
			var t = e.getFullYear();
			var n = e.getMonth() + 1;
			var n1 = n <= 9 ? "0" + n : n;
			var dd = e.getDate();
			var d1 = dd <= 9 ? "0" + dd : dd;
			var fulldate = t + "-" + n1 + "-" + d1;

			if (window.XMLHttpRequest) {
				xmlhttp_weekgraph = new XMLHttpRequest();
			} else {
				xmlhttp_weekgraph = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp_weekgraph.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					var all_data = null;
					all_data = eval(xmlhttp_weekgraph.responseText);
					console.log(all_data);
					for (var i = 0; i <= 6; i++) {
						for (var label in all_data[i]) {
							labels.push(label);
							for (var x in all_data[i][label]) {
								nutri_data[x].push(all_data[i][label][x]);
							}
						}
					}

					console.log(nutri_data);
					var myLineChart = new Chart(ctxD, {
						type: 'bar',

						data: {
							//labels: ["Carbohydrate", "Energy", "Fat", "Protine"],
							labels: labels,
							datasets: [{
									label: "Carbohydrate",
									barPercentage: 0.3,
									barThickness: 40,
									maxBarThickness: 50,
									minBarLength: 2,
									data: nutri_data["carb"],
									backgroundColor: "#F7464A",
									hoverBackgroundColor: "#FF5A5E",
								},
								{
									label: "Energy",
									barPercentage: 0.3,
									barThickness: 40,
									maxBarThickness: 50,
									minBarLength: 2,
									data: nutri_data["ene"],
									backgroundColor: "#46BFBD",
									hoverBackgroundColor: "#5AD3D1",
								},
								{
									label: "Fat",
									barPercentage: 0.3,
									barThickness: 40,
									maxBarThickness: 50,
									minBarLength: 2,
									data: nutri_data["fat"],
									backgroundColor: "#FDB45C",
									hoverBackgroundColor: "#FFC870",
								},
								{
									label: "Protine",
									barPercentage: 0.3,
									barThickness: 40,
									maxBarThickness: 50,
									minBarLength: 2,
									data: nutri_data["pro"],
									backgroundColor: "#949FB1",
									hoverBackgroundColor: "#A8B3C5",
								},

							]
						},
						options: {
							legend: {
								display: false,
							},
							title: {
								display: true,
								text: 'Nutrition Analysis'
							},
							scales: {
								xAxes: [{
									stacked: true
								}],
								yAxes: [{
									stacked: true
								}]
							},
						}
					});
					nutri_data = [];
					ctxD.clearRect(0, 0, canvas.width, canvas.height);
					
				}
			}



			xmlhttp_weekgraph.open("GET", "getdata.php?fulldate=" + fulldate + "&u_id=1&caller=getWeeklyData", true);
			xmlhttp_weekgraph.send();
		}
	</script>

</head>

<body onload="show()">
	<!-- <div class="chart_container">
			<h3 class="nutrition_head">Nutrition Analysis</h3>
			<div class="food_container">
				<input type="text" class="food-name">
				<button class="food-request" onclick="show()">Analyze</button>
			</div>
			<div class="loader" style="visibility: hidden;"></div>
			<div class="doughnautDisplay" >
				<canvas class="doughnutChart" ></canvas>
			</div>
		</div> -->


	<!-- Weekly and monthly view -->

	<div>
		<input type="radio" id="s1" name="s" checked />
		<input type="radio" id="s2" name="s" />
		<input type="radio" id="s3" name="s" />
		<input type="radio" id="s4" name="s" />
		<div class="tabs">
			<label for="s1">Weekly</label>
			<label for="s2">MOnthly</label>

		</div>
		<ul class="sections">
			<li>
				<div class="doughnautDisplay">
					<canvas class="doughnutChart"></canvas>
				</div>
			</li>
			<li>
				<div class="doughnautDisplay">
					<canvas class="doughnutChart"></canvas>
				</div>
			</li>

		</ul>
	</div>

</body>

<?php
date_default_timezone_set('Asia/Kathmandu');
echo "tehe date is".date('w');
$servername = "localhost";
$username = "root";
$password = "";
$db = "nutri";

// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

// Create database
$sql = "CREATE DATABASE IF NOT EXISTS nurti;";
if ($conn->query($sql) === TRUE) {
	echo "Database created successfully";
} else {
	echo "Error creating database: " . $conn->error;
}

$con = mysqli_connect("localhost","root","","nurti");
        $username = "rajnesh";
        $password = "1234rajnesh";

        $sql = "SELECT u_id FROM users WHERE username='".$username."' AND pass='".$password."'";

		$result = mysqli_query($con, $sql);
		
		if($result == null){
			echo "invalid";
		}

		else{

			$row = mysqli_fetch_array($result);
				echo $row['u_id'];
		}

$conn->close();
// echo "date is " . date("Y-m-d")


$xya = "3 kg rice";
 $multiplier = intval(explode(' ',trim($xya))[0]);
 $vvvv = "20";
$v = $multiplier * $vvvv; 
echo $v;
?>

</html>