
    <?php


    $caller = $_GET['caller'];

    if (isset($_GET['u_id']))
        $u_id = $_GET['u_id'];


    if ($caller == "setData") {
        setData($u_id);
    }

    if ($caller == "getTable") {
        getTable($u_id);
    }

    if ($caller == "getDailyNutri") {
        getDailyNutri($u_id);
    }

    if ($caller == "getWeeklyData") {
        getWeeklyData($u_id);
    }

    if ($caller == "checkUser") {
        checkUser();
    }

    if ($caller == "getAvg") {
        getAvg();
    }

    function setData($u_id)
    {
        date_default_timezone_set('Asia/Kathmandu');
        $q = json_decode($_GET['q']);

        #include('../dbhelper/createall.php');

        include('../dbhelper/connection.php');

        $stmt = $con->prepare("INSERT INTO nutri_data (foodname,fat,protein,energy,carbohydrate)
                VALUES (?, ?, ?, ?, ?)");

        $stmt->bind_param("sssss", $nam, $fat, $pro, $energy, $carbo);

        $nam = $q[4];
        $multiplier = intval(explode(' ',trim($nam))[0]);
        $fat = $multiplier * $q[2];
        $pro = $multiplier *  $q[3];
        $energy = $multiplier *  $q[1];
        $carbo = $multiplier *  $q[0];

        $stmt->execute();
        $stmt->close();


        $stmt = $con->prepare("INSERT INTO food_data (u_id,nutri_date,foodname)
                VALUES (?, ?, ?)");

        $stmt->bind_param("sss", $u_id, $date, $nam);

        $date = date("Y-m-d");

        $stmt->execute();
        $stmt->close();

        getTable($u_id);
    }

    function getTable($u_id)
    {
        date_default_timezone_set('Asia/Kathmandu');
        include('../dbhelper/connection.php');
        $fulldate = date('Y-m-d');
        if (isset($_GET['fulldate'])) {
            $fulldate = $_GET['fulldate'];
        }


        // $sql = "SELECT nd.foodname,carbohydrate,energy,fat,protein
        //         FROM nutri_data as nd
        //         INNER JOIN food_data as fd ON fd.foodname = nd.foodname WHERE nutri_date = '" . date('Y-m-d') . "' AND u_id=" . $_GET['u_id'];

        $sql = "SELECT nd.foodname,carbohydrate,energy,fat,protein
                FROM nutri_data as nd
                INNER JOIN food_data as fd ON fd.foodname = nd.foodname WHERE nutri_date = '" . $fulldate . "' AND u_id=" . $_GET['u_id'];

        $result = mysqli_query($con, $sql);

        echo "<table border=1px>
                <tr>
                <th>Sno.</th>
                <th>Foodname</th>
                <th>Carbohydrate</th>
                <th>Energy</th>
                <th>Fat</th>
                <th>Protein</th>
                </tr>";
        $i = 0;
        while ($row = mysqli_fetch_array($result)) {
            $i++;
            echo "<tr>";
            echo "<td>" . $i . "</td>";

            echo "<td>" . $row['foodname'] . "</td>";
            echo "<td>" . $row['carbohydrate'] . "</td>";
            echo "<td>" . $row['energy'] . "</td>";
            echo "<td>" . $row['fat'] . "</td>";
            echo "<td>" . $row['protein'] . "</td>";

            echo "</tr>";
        }
        echo "</table>";
        $con->close();
    }


    function getDailyNutri($u_id)
    {
        include('../dbhelper/connection.php');
        date_default_timezone_set('Asia/Kathmandu');

        $fulldate = date('Y-m-d');
        if (isset($_GET['fulldate'])) {
            $fulldate = $_GET['fulldate'];
        }

        $sql = "SELECT carbohydrate,energy,fat,protein
        FROM nutri_data as nd
        INNER JOIN food_data as fd ON fd.foodname = nd.foodname WHERE nutri_date = '" . $fulldate . "' AND u_id=" . $_GET['u_id'];

        $result = mysqli_query($con, $sql);

        $x = array();
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;

        while ($row = mysqli_fetch_array($result)) {
            $a = $a + (int) $row['carbohydrate'];
            $b = $b + (int) $row['energy'];
            $c = $c + (int) $row['fat'];
            $d = $d + (int) $row['protein'];
        }

        array_push($x, $a, $b, $c, $d);

        echo json_encode($x);

        $con->close();
    }


    function getWeeklyData($u_id)
    {
        date_default_timezone_set('Asia/Kathmandu');
        include('../dbhelper/connection.php');
        $dow_numeric = date('w');
        $dow_text = date('D', strtotime("Sunday +{$dow_numeric} days"));

        $weekdays = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
        $all = array();
        $x = $dow_numeric + 1;
        $counter = 1;
        while ($counter <= 7) {
            array_push($all, array($weekdays[$x] => array("crab" => "", "ene" => "", "fat" => "", "pro" => "")));

            $x = ($x + 1) % 7;
            $counter += 1;
        }

        $fulldate = date('Y-m-d');
        if (isset($_GET['fulldate'])) {
            $fulldate = $_GET['fulldate'];
        }

        $sql = "SELECT carbohydrate,energy,fat,protein
        FROM nutri_data as nd
        INNER JOIN food_data as fd ON fd.foodname = nd.foodname WHERE nutri_date = '" . $fulldate . "' AND u_id=" . $_GET['u_id'];

        $result = mysqli_query($con, $sql);

        $x = array();
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;

        while ($row = mysqli_fetch_array($result)) {
            $a = $a + (int) $row['carbohydrate'];
            $b = $b + (int) $row['energy'];
            $c = $c + (int) $row['fat'];
            $d = $d + (int) $row['protein'];
        }

        $all[6][$weekdays[$dow_numeric]]["crab"] = $a;
        $all[6][$weekdays[$dow_numeric]]["ene"] = $b;
        $all[6][$weekdays[$dow_numeric]]["fat"] = $c;
        $all[6][$weekdays[$dow_numeric]]["pro"] = $d;


        $prev_date = prev_date($fulldate);

        $sql = "SELECT carbohydrate,energy,fat,protein
        FROM nutri_data as nd
        INNER JOIN food_data as fd ON fd.foodname = nd.foodname WHERE nutri_date = '" . $prev_date . "' AND u_id=" . $_GET['u_id'];

        $result = mysqli_query($con, $sql);

        $x = array();
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;

        while ($row = mysqli_fetch_array($result)) {
            $a = $a + (int) $row['carbohydrate'];
            $b = $b + (int) $row['energy'];
            $c = $c + (int) $row['fat'];
            $d = $d + (int) $row['protein'];
        }

        $dow_numeric = $dow_numeric - 1;
        if ($dow_numeric < 0) {
            $dow_numeric = 6;
        }
        $all[5][$weekdays[$dow_numeric]]["crab"] = $a;
        $all[5][$weekdays[$dow_numeric]]["ene"] = $b;
        $all[5][$weekdays[$dow_numeric]]["fat"] = $c;
        $all[5][$weekdays[$dow_numeric]]["pro"] = $d;





        $prev_date = prev_date($prev_date);


        $sql = "SELECT carbohydrate,energy,fat,protein
        FROM nutri_data as nd
        INNER JOIN food_data as fd ON fd.foodname = nd.foodname WHERE nutri_date = '" . $prev_date . "' AND u_id=" . $_GET['u_id'];

        $result = mysqli_query($con, $sql);

        $x = array();
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;

        while ($row = mysqli_fetch_array($result)) {
            $a = $a + (int) $row['carbohydrate'];
            $b = $b + (int) $row['energy'];
            $c = $c + (int) $row['fat'];
            $d = $d + (int) $row['protein'];
        }

        $dow_numeric = $dow_numeric - 1;
        if ($dow_numeric < 0) {
            $dow_numeric = 6;
        }
        $all[4][$weekdays[$dow_numeric]]["crab"] = $a;
        $all[4][$weekdays[$dow_numeric]]["ene"] = $b;
        $all[4][$weekdays[$dow_numeric]]["fat"] = $c;
        $all[4][$weekdays[$dow_numeric]]["pro"] = $d;






        $prev_date = prev_date($prev_date);

        $sql = "SELECT carbohydrate,energy,fat,protein
        FROM nutri_data as nd
        INNER JOIN food_data as fd ON fd.foodname = nd.foodname WHERE nutri_date = '" . $prev_date . "' AND u_id=" . $_GET['u_id'];

        $result = mysqli_query($con, $sql);

        $x = array();
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;

        while ($row = mysqli_fetch_array($result)) {
            $a = $a + (int) $row['carbohydrate'];
            $b = $b + (int) $row['energy'];
            $c = $c + (int) $row['fat'];
            $d = $d + (int) $row['protein'];
        }

        $dow_numeric = $dow_numeric - 1;
        if ($dow_numeric < 0) {
            $dow_numeric = 6;
        }
        $all[3][$weekdays[$dow_numeric]]["crab"] = $a;
        $all[3][$weekdays[$dow_numeric]]["ene"] = $b;
        $all[3][$weekdays[$dow_numeric]]["fat"] = $c;
        $all[3][$weekdays[$dow_numeric]]["pro"] = $d;







        $prev_date = prev_date($prev_date);

        $sql = "SELECT carbohydrate,energy,fat,protein
        FROM nutri_data as nd
        INNER JOIN food_data as fd ON fd.foodname = nd.foodname WHERE nutri_date = '" . $prev_date . "' AND u_id=" . $_GET['u_id'];

        $result = mysqli_query($con, $sql);

        $x = array();
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;

        while ($row = mysqli_fetch_array($result)) {
            $a = $a + (int) $row['carbohydrate'];
            $b = $b + (int) $row['energy'];
            $c = $c + (int) $row['fat'];
            $d = $d + (int) $row['protein'];
        }

        $dow_numeric = $dow_numeric - 1;
        if ($dow_numeric < 0) {
            $dow_numeric = 6;
        }
        $all[2][$weekdays[$dow_numeric]]["crab"] = $a;
        $all[2][$weekdays[$dow_numeric]]["ene"] = $b;
        $all[2][$weekdays[$dow_numeric]]["fat"] = $c;
        $all[2][$weekdays[$dow_numeric]]["pro"] = $d;







        $prev_date = prev_date($prev_date);

        $sql = "SELECT carbohydrate,energy,fat,protein
        FROM nutri_data as nd
        INNER JOIN food_data as fd ON fd.foodname = nd.foodname WHERE nutri_date = '" . $prev_date . "' AND u_id=" . $_GET['u_id'];

        $result = mysqli_query($con, $sql);

        $x = array();
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;

        while ($row = mysqli_fetch_array($result)) {
            $a = $a + (int) $row['carbohydrate'];
            $b = $b + (int) $row['energy'];
            $c = $c + (int) $row['fat'];
            $d = $d + (int) $row['protein'];
        }

        $dow_numeric = $dow_numeric - 1;
        if ($dow_numeric < 0) {
            $dow_numeric = 6;
        }
        $all[1][$weekdays[$dow_numeric]]["crab"] = $a;
        $all[1][$weekdays[$dow_numeric]]["ene"] = $b;
        $all[1][$weekdays[$dow_numeric]]["fat"] = $c;
        $all[1][$weekdays[$dow_numeric]]["pro"] = $d;








        $prev_date = prev_date($prev_date);

        $sql = "SELECT carbohydrate,energy,fat,protein
        FROM nutri_data as nd
        INNER JOIN food_data as fd ON fd.foodname = nd.foodname WHERE nutri_date = '" . $prev_date . "' AND u_id=" . $_GET['u_id'];

        $result = mysqli_query($con, $sql);

        $x = array();
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;

        while ($row = mysqli_fetch_array($result)) {
            $a = $a + (int) $row['carbohydrate'];
            $b = $b + (int) $row['energy'];
            $c = $c + (int) $row['fat'];
            $d = $d + (int) $row['protein'];
        }

        $dow_numeric = $dow_numeric - 1;
        if ($dow_numeric < 0) {
            $dow_numeric = 6;
        }
        $all[0][$weekdays[$dow_numeric]]["crab"] = $a;
        $all[0][$weekdays[$dow_numeric]]["ene"] = $b;
        $all[0][$weekdays[$dow_numeric]]["fat"] = $c;
        $all[0][$weekdays[$dow_numeric]]["pro"] = $d;



        echo json_encode($all);
        //echo json_encode($dow_numeric);

        $con->close();
    }



    function prev_date($prev_date)
    {
        $date = strtotime("-1 day", strtotime($prev_date));
        return date("Y-m-d", $date);
    }


    function checkUser()
    {
        date_default_timezone_set('Asia/Kathmandu');
        include('../dbhelper/connection.php');
        $username = $_GET['username'];
        $password = $_GET['password'];

        $sql = "SELECT u_id
        FROM users WHERE username='" . $username . "' AND pass='" . $password . "'";

        $result = mysqli_query($con, $sql);

        if (mysqli_num_rows($result)>0) {
            $row = mysqli_fetch_array($result);
            echo json_encode($row['u_id']);
        } else {
            echo json_encode("invalid");
        }
    }


    function getAvg(){
        include('../dbhelper/connection.php');
        date_default_timezone_set('Asia/Kathmandu');

        $fulldate = date('Y-m-d');
        if (isset($_GET['fulldate'])) {
            $fulldate = $_GET['fulldate'];
        }

        $sql = "SELECT carbohydrate,energy,fat,protein
        FROM nutri_data as nd
        INNER JOIN food_data as fd ON fd.foodname = nd.foodname WHERE nutri_date = '" . $fulldate . "' AND u_id=" . $_GET['u_id'];

        $result = mysqli_query($con, $sql);

        $x = array();
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;

        while ($row = mysqli_fetch_array($result)) {
            if($row['carbohydrate'] > 0)
                $a += 1;
            if($row['energy'] > 0)
                $b += 1;
            if($row['fat'] > 0)
                $c +=1;
            if($row['protein'] > 0)
                $d +=1;
        }

        array_push($x, $a, $b, $c, $d);

        echo json_encode($x);

        $con->close();
    }

    ?>
