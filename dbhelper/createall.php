<?php

    $servername = "localhost";
    $username = "root";
    $password = "";
    $db="";

    $con = mysqli_connect($servername,$username,$password,$db);

    $sql = "CREATE DATABASE IF NOT EXISTS nurti;";
    if ($con->query($sql) === TRUE) {
        $con = mysqli_connect($servername,$username,$password,"nurti");
        $sql = "CREATE TABLE IF NOT EXISTS users (u_id int PRIMARY KEY,
        first_name varchar(30) NOT NULL,
        last_name varchar(30) NOT NULL,
        contact varchar(10),
        address varchar(30),
        username varchar(30) NOT NULL UNIQUE (u_id),
        pass varchar(30) NOT NULL)";
        if ($con->query($sql) === TRUE) {
            $sql = "CREATE TABLE IF NOT EXISTS nutri_data (foodname varchar(30) PRIMARY KEY,
            fat varchar(10),
            protein varchar(10),
            energy varchar(10),
            carbohydrate varchar(10))";
            if ($con->query($sql) === TRUE) {
                $sql = "CREATE TABLE IF NOT EXISTS food_data (u_id int,
                nutri_date varchar(30),
                foodname varchar(30),
                FOREIGN KEY (u_id) REFERENCES users(u_id),
                FOREIGN KEY (foodname) REFERENCES nutri_data(foodname))";
                $con->query($sql);
            }
        }
    } else {
        echo "Error creating database: " . $con->error;
    }

?>